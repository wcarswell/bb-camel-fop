# Camel Fop Example

### Introduction

This example shows how to use Apache to route from xml->pdf using fop and xslt


### Build
You will need to compile this example first:

	mvn clean install

### Run
	mvn jetty:run
