<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.1"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:fo="http://www.w3.org/1999/XSL/Format">
<xsl:output encoding="iso-8859-1" />
<xsl:template match ="services">
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
        <fo:layout-master-set>
            <fo:simple-page-master master-name="service">
                <fo:region-body></fo:region-body>
            </fo:simple-page-master>
        </fo:layout-master-set>

        <fo:page-sequence master-reference="service">
            <fo:flow flow-name="xsl-region-body">
                <fo:block>
                    <fo:table>
                        <fo:table-body>
                            <fo:table-row>
                                <fo:table-cell border="solid 1px black" 
                                text-align="center" font-weight="bold">
                                    <fo:block>Service Id</fo:block>
                                </fo:table-cell>
                                <fo:table-cell border="solid 1px black" 
                                text-align="center" font-weight="bold">
                                    <fo:block>Open Date</fo:block>
                                </fo:table-cell>
                                <fo:table-cell border="solid 1px black" 
                                text-align="center" font-weight="bold">
                                    <fo:block>Close Date</fo:block>
                                </fo:table-cell>                                    
                                <fo:table-cell border="solid 1px black" 
                                text-align="center" font-weight="bold">
                                    <fo:block>Order Status</fo:block>
                                </fo:table-cell>                                    
                                <fo:table-cell border="solid 1px black" 
                                text-align="center" font-weight="bold">
                                    <fo:block>Description</fo:block>
                                </fo:table-cell>
                                <fo:table-cell border="solid 1px black" 
                                text-align="center" font-weight="bold">
                                    <fo:block>Repair Cost</fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <xsl:for-each select="./service">
                                <fo:table-row>
                                    <fo:table-cell border="solid 1px bold" text-align="center">
                                        <fo:block><xsl:value-of select="serviceId" /></fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell border="solid 1px bold" text-align="center">
                                        <fo:block><xsl:value-of select="openDate" /></fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell border="solid 1px bold" text-align="center">
                                        <fo:block><xsl:value-of select="closeDate" /></fo:block>
                                    </fo:table-cell>                                
                                    <fo:table-cell border="solid 1px bold" text-align="center">
                                        <fo:block><xsl:value-of select="orderStatus" /></fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell border="solid 1px bold" text-align="center">
                                        <fo:block><xsl:value-of select="description" /></fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell border="solid 1px bold" text-align="center">
                                        <fo:block><xsl:value-of select="repairCost" /></fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                            </xsl:for-each>
                        </fo:table-body>
                    </fo:table>
                </fo:block>
            </fo:flow>
        </fo:page-sequence>
    </fo:root>
</xsl:template>
</xsl:stylesheet>